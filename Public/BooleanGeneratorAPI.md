# WiseStep | Boolean Query Builder | Documentation

---
> Build comprehensive Boolean Search Queries automatically



## Table of Contents

### 1. [Overview](#overview)
- [Server URL](#server-url)


### 2. [Endpoint](#endpoint)


    
### 4. [Errors](#errors)

### 5. [Usage](#usage)

- [Python](#python)



---



# Overview

> Boolean Query Builder is a simple REST API. The API is capable of generating the following variations of Boolean Search Strings :

The API has 2 modes:
- `quick`
- `jobdescription`

### Server URL
 
Server URL for all the endpoints is ```https://recruiter.wisestep.com/ws_ai_infinity```


 ---


# Authentication



> All API requests should include your API key in an Authorization HTTPS header as follows:

```

 Authorization : Bearer API_KEY_HERE 

```

Steps to get an API KEY :

> COMING SOON ...


---

# Query Builder

> The query builder endpoints allow you to generate query either from ```quick``` mode or ```jobdescription``` mode.


----


``` POST https://recruiter.wisestep.com/ws_ai_infinity/wisestep-parsers/boolean_query_builder/ ```

### Request Body 

---

``` mode``` [string] *REQUIRED*

 > Mode in which query should be made.
    ```quick``` or ```jobdescription```
 

``` jobTitle``` [string] *REQUIRED*

> Required if mode chosen is ```quick``` else optional.

``` required_skills``` [array] *OPTIONAL*

> List of skills that are mandatory.

``` query_type``` [array] *REQUIRED*

> Type of query to generate among

- ```focused``` - Short query, focused only on skills in JD.

- ```open``` - Medium sized query, adds similar skills to the query. ```DEFAULT```

- ```comprehensive``` - Largest query, adds similar job titles as well as similar skills as found in JD.

``` text ``` [string] *OPTIONAL*

> Job description to generate query for.

 - REQUIRED for mode=```jobdescription```, else OPTIONAL.


---

### Response Object

---

``` booleanQuery``` [dictionary]

 > Dictionary holding the query
 

``` mode``` [string]

> Mode chosen

``` query```[dictionary]

> Boolean Query

---

## Usage

###### Python

```

import requests
import json

url = <URL>
payload_quick_mode = json.dumps({"mode":"quick","query_type":"open","jobTitle":"data scientist","required_skills":["python","java"]})

payload_jd_mode = json.dumps({"mode":"quick","query_type":"open","jobTitle":"data scientist","text":<JD>,"required_skills":["python","java"]})

headers = {
  'Authorization': 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJtYW5pc2guZ3JvdmVyQG5vcnRoaGlsbHBhcnRuZXJzLmNvbSIsImF1dGgiOiJST0xFX1VTRVIiLCJvbGRFbWFpbHMiOiIiLCJleHAiOjE2Mjc0NzY0Mjd9.ygxIl0eMBVeJjW0lKoJityJ6qmWIcM-MKyP0vuTMnFRtNLnOC4N3oglbFaQaJAo7_2bJ4Ik4FvLrIGKbKmIDyA',
  'Content-Type': 'application/json'
}

response = requests.request("POST", url, headers=headers, data=payload)

print(response.text)

```