# WiseStep AI Infinity | Documentation

---

> In case of any problem, please contact Team Jarvis | WiseStep



## Table of Contents

### 1. [Overview](#overview)
- [Server URL](#server-url)

### 2. [Authentication](#authentication)

### 3. [Endpoints](#endpoints)

- [Lookup](#search-endpoints)
    
        
- [Recommendation Engines](#recommendation-engine)
    - [Similar](#recommendation-engine-similar)
    - [Related](#recommendation-engine-related)
    
    ---
    
- [Parsers](#parsers)
    - [Resume Parser](#resume-parser)
    - [Job Description Parser](#jd-parser)
        
    ---
    
### 4. [Errors](#errors)

### 5. [Usage](#usage)

- [Python](#python)
    
- [cURL](#cURL)


### [Endpoint Index](#endpoint-index)
    
### 6. [Limits](#limits)
    

---


<a name='overview'></a>

# Overview

> WiseStep AI Infinity is a simple HTTPS REST API. The API is capable of doing a variety of tasks like: 

 - Verifying if a job title exists
 - Verifying if a company name exists
 - Verifying if a skill exists
 - Recommending similar job titles
 - Recommending similar skills
 - Extracting information from Resume like, Name, skills, address, education, experiences, college, degrees, & much more
 - Extracting information from Job descriptions like, Salary range, currency, job title, client, location, & much more
 
<a name='server-url'></a>
### Server URL
 
Server URL for all the endpoints is ```https://recruiter.wisestep.com/ws_ai_infinity```


 ---
 
<a name='authentication'></a>

# Authentication



> All API requests should include your API key in an Authorization HTTPS header as follows:

<pre>

 Authorization : Bearer API_KEY_HERE 

</pre>

Steps to get an API KEY :

> COMING SOON ...


---

<a name='search-endpoints'></a>
# Lookup

> The lookup endpoints allow you to search in WiseStep's large database of skill, jobtitles, & company names to see if the token in correct.


----


``` POST https://recruiter.wisestep.com/ws_ai_infinity/lookup_api/redis_lookup/ ```

### Request Body 

---

``` attribute``` <i>string</i> <strong>REQUIRED</strong>
 > Attribute to search for among 
 ```jobTitle``` , ```skill```, & ```companyName```
 

``` token_list``` <i>array</i> <strong>REQUIRED</strong>
> One or more tokens to search for


---

### Response Object

---

``` count_list``` <i>dictionary</i> 
 > Dictionary holding the results
 

``` count``` <i>string</i> 
> A variable defining how likely the token is a valid
``` attribute```. Bigger the number, more likely the validity.

``` attribute``` <i>string</i>
> Token searched for.


---

<a name='recommendation-engine'></a>

# Recommendation Engine

> The recommendation endpoints allow you to use WiseStep's recommendation algorithms to sugges skill, jobtitles, & company names similar or related to the query token.


----

<a name='recommendation-engine-similar'></a>


## Similar


``` POST https://recruiter.wisestep.com/ws_ai_infinity/ai_recommendation_engine/similar_attribute/	 ```

### Request Body 

---

``` attribute``` <i>string</i> <strong>REQUIRED</strong>
 > Attribute to search for among 
 ```jobTitle``` , ```skill```, & ```companyName```
 

``` keyword``` <i>string</i> <strong>REQUIRED</strong>
> Single tokens to generate suggestions for


---

### Response Object


----

<a name='recommendation-engine-related'></a>


## Related


``` POST https://recruiter.wisestep.com/ws_ai_infinity/ai_recommendation_engine/related_attribute/	 ```

### Request Body 

---

``` attribute``` <i>string</i> <strong>REQUIRED</strong>
 > Attribute to search for among 
 ```jobTitle``` , ```skill```, & ```companyName```
 

``` input_list``` <i>array</i> <strong>REQUIRED</strong>
> One or more tokens to generate suggestions for


---

### Response Object

---


<a name='parsers'></a>

# Parsers

> The parsers endpoints allow you to extract information using WiseStep's extraction algorithms for resumes, and job descriptions


----


<a name='jd-parser'></a>

## JD Parser

``` POST https://recruiter.wisestep.com/ws_ai_infinity/wisestep-parsers/jd-parser/```


### Request Body 

---

``` text``` <i>string</i> <strong>REQUIRED</strong>
 > Job Description text
 

---

### Response Object

---

``` parsed_jd``` <i>dictionary</i> 
 > Dictionary holding the results
 

``` clientName``` <i>string</i> 
> Client name as found in 
```text```

``` compensations``` <i>array</i>
> Information about compensation as found in 
```text``` which includes:

- ```currencies``` <i>array</i>
    > Currency as found in text
    
- ```details``` <i>array</i>
> Details about compensations as found in ```text``` which includes:
    
* ```type```<i>string</i>
  > Type of compensation among <pre>per year</pre> <pre>per month</pre> <pre>per hour</pre> <pre>NA(if not found)</pre>
        
    * ```value```<i>string</i>
  > Amount as found in ```text```
        
- ```ranges``` <i>dictionary</i>
 > Mininum and maximum amount found in text
 
* ```max``` <i>int</i>
> Maximum amount for compensation as found in ```text```
* ```min```<i>int</i>
  > Minimum amount for compensation as found in ```text```
         
         
- ```hiringManager```<i>array</i>
 > Hiring Manager name as found in text
 
 
- ```jobDescription```<i>array</i>
> Array of the most important pieces of text

- ```jobTitles``` <i>array</i>
> All the job titles as found in text

- ```jobType``` <i>array</i>
> Job Types as found in text among <pre>contract</pre> <pre>permanent</pre> <pre>fulltime</pre> <pre>parttime</pre>
        
        
- ```locations``` <i>array</i>
> Job locations as found in ```text```

- ```skills``` <i>array</i>
> Skills as found in ```text```

- ```workAuthorization``` <i>array</i>
> Visatype required details as found in text


<a name='resume-parser'></a>

## Resume Parser

``` POST https://recruiter.wisestep.com/ws_ai_infinity/wisestep-parsers/resume-parser/```


### Request Body 

---

``` text``` <i>string</i> <strong>REQUIRED</strong>
 > Resume text
 

---

### Response Object

---

``` resume_content``` <i>string</i> 
 > Text that was inputted
 

``` personal_information``` <i>dictionary</i> 
> Details about personal information that as found in ```text``` which includes 


* ``` emails``` <i>string</i> 
> Candidate Email as found in ```text``` 

* ```contact_number``` <i>dictionary</i>
> Contact numbers as found in ```text```
* ``` primary ``` <i>string</i>
> Primary phone number as found in ```text```
* ```secondary``` <i>string</i>
> Secondary phone number as found in ``text``
    
- ```profile_urls``` <i>dictionary</i>
> Social media URLs as found in ```text``` which includes:
    
- ```linkedin``` <i>string</i>
> LinkedIn URL
* ```github``` <i>string</i>
> GitHub URL

* ```Location``` <i>dictionary</i>
> Information about location as found in ```text``` which inclueds:

* ```addressLine``` <i>string</i>
> Text containing address
  
* ```city``` <i>string</i>
> City as found in ```text```
  

* ```state``` <i>state</i>
> State as predicted from ```text```
* ```stateCode``` <i>string</i>
> State Code as predicted from ```text```
* ```country``` <i>string</i>
> Country as predicted from ```text```
* ```countryCode``` <i>string</i>
> Country Code as predicted from ```text```
* ```continent``` <i>string</i>
> Continent as predicted from ```text```
* ```zipCode``` <i>string</i>
> City as found in ```text```
* ```lat``` <i>float</i>
> Latitude as predicted from ```text```
* ```long``` <i>float</i>
> Longitude as predicted in ```text```
* ```point``` <i>string</i>
> ```lat``` ```long ``` expressed as single entity
* ```area``` <i>string</i>
> Area
* ```name``` <i>string</i>
> Name of the candidate
 

```education``` <i>array</i>
> List of education details as found in ```text``` which includes:

* ```college``` <i>string</i>
> College name as found in ```text```
* ```description``` <i>string</i>
> Part of ```text``` used as reference to extract education information
* ```startDate``` <i>string</i>
> Start of education as found in ```text```
* ```endDate``` <i>string</i>
> End of education as found in ```text```
* ```location``` <i>string</i>
> Predicted location of educational institute
* ```degree``` <i>string</i>
> Degree as found in ```text```
* ```department``` <i>string</i>
> Predicted department of ```degree```
* ```isCurrentEd``` <i>boolean</i>
> Set ```True``` if pursuing education, else ```False```

```experience``` <i>array</i>
> Information about work experience as found in ```text``` which includes:



* ```date``` <i>string</i>
> Date as found in ```text```
* ```job_title``` <i>array</i>
> Job title for the work experience
* ```company_name``` <i>string</i>
> Company name for the work experience
* ```start_date``` <i>array</i>
> Start date of work at ```company_name``` as ```job_title```
* ```end_date``` <i>array</i>
> End date of work at ```company_name``` as ```job_title```
* ```is_current_exp``` <i>boolean</i>
> Set ```True``` if still in role, else ```False```
* ```number_of_days``` <i>int</i>
> Number of days spent at ```job_title``` in ```company_name```
* ```skills``` <i>array</i>
> Skills in ```job_title``` at ```company_name```
* ```content``` <i>string</i>
> Part of ```text``` used as reference to extract work experience


```certification``` <i>array</i>
> List of certifications, awards, & publications


 


    
    
    
<a name = 'errors'></a>

# Errors

> If you make a bad request we'll let you know by returning a relevant HTTPS status code.


| Status Code | Meaning                                    | Troubleshooting                |
|-------------|--------------------------------------------|--------------------------------|
| 200         | Successful request and response.           | OK                             |
| 400         | Malformed parameters or other bad request. | Check payload key-value format |
| 403         | Forbidden, Unauthorised                    | Check Authentication key-value |
| 404         | Not found                                  | Check endpoints, URL           |


---
----

<a name = 'usage'></a>

# Usage

<a name = 'curl'></a>
#### cURL

```

curl  --request POST 'https://recruiter.wisestep.com/ws_ai_infinity/<endpoint>' \
--header 'Authorization: Bearer <jhi-authentication-value>' \
--header 'Content-Type: application/json' \
--data-raw '{
    "key1":"value1",
    "key2":"value2"
    }'

```
---
<a name = 'python'></a>
#### Python3

```

import requests
import json

url = "https://recruiter.wisestep.com/ws_ai_infinity/<endpoint>"

payload = json.dumps({
  "key1": "value1",
  "key2": "value2"
})
headers = {
  'Authorization': 'Bearer <jhi-authentication-value',
  'Content-Type': 'application/json'
}

response = requests.request("POST", url, headers=headers, data=payload)
print(response.text)

```


<a name = 'endpoint-index'></a>

# Endpoint Index

> A quick look at all the endpoints


## Lookup

| Description                             	| Endpoint                  	| Request Body                                        	| Response Example                                                          	|
|-----------------------------------------	|---------------------------	|-----------------------------------------------------	|---------------------------------------------------------------------------	|
| Check if a skill exists in WisetStep database        	| /lookup_api/redis_lookup/ 	| ```{"attribute":"skill", "token_list": []}```       	| ``` {"count_list": [{"count": "3149","skill": "python"}]}```              	|
| Check if a job title exists in WisetStep database    	| /lookup_api/redis_lookup/ 	| ```{"attribute":"jobTitle", "token_list": []}```    	| ``` {"count_list": [{"count": "3149","jobTitle": "python developer"}]}``` 	|
| Check if a company name exists in WisetStep database 	| /lookup_api/redis_lookup/ 	| ```{"attribute":"companyName", "token_list": []}``` 	| ``` {"count_list": [{"count": "3149","companyName": "Infosys"}]}```       	|



## Recommendation Engines (similar)

| Description               	| Endpoint                                    	| Request Body                                      	| Response Example 	|
|---------------------------	|---------------------------------------------	|---------------------------------------------------	|------------------	|
| Get Similar skills        	| /ai_recommendation_engine/similar_attribute/ 	| ```{"attribute":"skill", "keyword": str}```       	|                  	|
| Get Similar job titles    	| /ai_recommendation_engine/similar_attribute/ 	| ```{"attribute":"jobTitle", "keyword": str}```    	|                  	|
| Get Similar company names 	| /ai_recommendation_engine/similar_attribute/ 	| ```{"attribute":"companyName", "keyword": str}``` 	|                  	|


## Recommendation Engines (related)
| Description               	| Endpoint                                    	| Request Body                                        	| Response Example 	|
|---------------------------	|---------------------------------------------	|-----------------------------------------------------	|------------------	|
| Get related skills        	| /ai_recommendation_engine/related_attribute/ 	| ```{"attribute":"skill", "input_list": []}```       	|                  	|
| Get related job titles    	| /ai_recommendation_engine/related_attribute/ 	| ```{"attribute":"jobTitle", "input_list": []}```    	|                  	|
| Get related company names 	| /ai_recommendation_engine/related_attribute/ 	| ```{"attribute":"companyName", "input_list": []}``` 	|                  	|


## Parsers

| Description                   | Endpoint                     | Request Body                                                                                                  | Response Example                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
|-------------------------------|------------------------------|---------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| API to parse job descriptions | /wisestep-parsers/jd-parser/ | ```{"text":"software engineer on contract with 45k-60k per year at tech mahindra in austin texas with L1 visa."}``` | ```{'parsed_jd': {'clientName': ['tech mahindra'],   'compensations': [{'currencies': ['USD'],     'details': [{'type': 'NA', 'value': ' 60k'},      {'type': 'NA', 'value': ' 45k'}],     'ranges': {'max': 60000, 'min': 45000}}],   'hiringManager': [' '],   'jobDescription': ['Job Description: \n'],   'jobTitles': ['software engineer'],   'jobType': ['contract'],   'locations': [],   'skills': ['software engineer', 'texas', 'l1'],   'workAuthorization': ['h', 'h 4', 'l', 'l1']}}``` |

