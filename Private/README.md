<h1 style="text-align:center">WiseStep Data Science API | Documentation </h1>

---

> In case of any problem, please contact Team Jarvis | WiseStep


## Table of contents

1. [Overview](#Overview)

---

2. [Content Type](#Content-type)

---

3. [Authentication](#Authentication)

---

4. [General Request Formats](#General-Request-Format)
    - [cURL](#cURL)
    - [Python3](#Python3)
    - [Java](#Java)
    - [NodeJS](#NodeJS)
   
--- 

5. [Lookup Endpoints](#Lookup-Endpoints)
    - [Redis Lookup Endpoints](#Redis-Lookup-Endpoints)
    - [ElasticSearch Lookup Endpoints](#ElasticSearch-Lookup-Endpoints)
    - [Recommendation Engine Endpoints(Similar)](#Recommendation-Engine-Endpoints-(Similar))
    - [Recommendation Engine Endpoints(Related)](#Recommendation-Engine-Endpoints-(Related))
   
---

6. [Parsers](#Parsers)

---

7. [Errors](#Errors)


<a name = 'Overview'></a>
## Overview

WiseStep's complete collection of Data Science APIs. These APIs exposes all of our data and algorithms.

---

<a name = 'Content-type'></a>
## Content type

Unless otherwise noted, all requests that require a body accept application/json. Likewise, all response bodies are application/json.

---

<a name = 'Authentication'></a>
## Authentication

All endpoints require a bearer token. Tokens are granted through the following steps :

* Login to a workspace by clicking [here](https://avanceconsulting.wisestep.com/) or copy-pasting the following URL to your browser : ```https://avanceconsulting.wisestep.com/```

---

* Right click and select ```Inspect Element``` or ```Inspect```.
![Inspect Element](../images/inspectElementChrome.png)

---


* This will open up a side panel in your window. Click on the 2 arrows (```>>```) and select ```Application```.
![Application](../images/ApplicationChrome.png)

---


* Select ```Local Storage``` and select the first URL (must be in the format **.wisestep.com). Copy the value of <strong>jhi-authenticationToken<strong>
    
![Auth](../images/AuthChrome.png)    


<hr style="border-top: 1px solid black;">


<a name = 'General-Request-Format'></a>
## General Request Format

---

<a name = 'cURL'></a>
#### cURL

```

curl  --request POST 'https://recruiter.wisestep.com/ws_ai_infinity/<endpoint>' \
--header 'Authorization: Bearer <jhi-authentication-value>' \
--header 'Content-Type: application/json' \
--data-raw '{
    "key1":"value1",
    "key2":"value2"
    }'

```
---
<a name = 'Python3'></a>
#### Python3

```

import requests
import json

url = "https://recruiter.wisestep.com/ws_ai_infinity/<endpoint>"

payload = json.dumps({
  "key1": "value1",
  "key2": "value2
})
headers = {
  'Authorization': 'Bearer <jhi-authentication-value',
  'Content-Type': 'application/json'
}

response = requests.request("POST", url, headers=headers, data=payload)
print(response.text)

```
---
<a name = 'Java'></a>
#### Java

```

Unirest.setTimeouts(0, 0);
HttpResponse<String> response = Unirest.post("https://recruiter.wisestep.com/ws_ai_infinity/<endpoint>")
  .header("Authorization", "Bearer <jhi-authentication-value")
  .header("Content-Type", "application/json")
  .body("{\n    \"key1\":\"value1\",\n    \"key2\":\"value2\")
  .asString();
  
```
---
<a name = 'NodeJS'></a>
#### NodeJS

```

var request = require('request');
var options = {
  'method': 'POST',
  'url': 'https://recruiter.wisestep.com/ws_ai_infinity/<endpoint>',
  'headers': {
    'Authorization': 'Bearer <jhi-authentication-value>',
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    "key1": "value1",
    "key2": "value2"
  })

};
request(options, function (error, response) {
  if (error) throw new Error(error);
  console.log(response.body);
});

```
<hr style="border-top: 1px solid black;">



<a name = 'Lookup-Endpoints'></a>
## Lookup Endpoints




<a name = 'Redis-Lookup-Endpoints'></a>
<h1 style="text-align:center">Redis Lookup Endpoints</h1>

---

<h4 style="text-align:center"> Check if tokens are present in redis database</h4>

| Description                             	| Endpoint                  	| Request Body                                        	| Response Example                                                          	|
|-----------------------------------------	|---------------------------	|-----------------------------------------------------	|---------------------------------------------------------------------------	|
| Check if a skill exists in Redis        	| /lookup_api/redis_lookup/ 	| ```{"attribute":"skill", "token_list": []}```       	| ``` {"count_list": [{"count": "3149","skill": "python"}]}```              	|
| Check if a job title exists in Redis    	| /lookup_api/redis_lookup/ 	| ```{"attribute":"jobTitle", "token_list": []}```    	| ``` {"count_list": [{"count": "3149","jobTitle": "python developer"}]}``` 	|
| Check if a company name exists in Redis 	| /lookup_api/redis_lookup/ 	| ```{"attribute":"companyName", "token_list": []}``` 	| ``` {"count_list": [{"count": "3149","companyName": "Infosys"}]}```       	|


<hr style="border-top: 1px solid black;">




<a name = 'ElasticSearch-Lookup-Endpoints'></a>
<h1 style="text-align:center">ElasticSearch Lookup Endpoints</h1>

---

<h4 style="text-align:center"> Check if tokens are present in ElasticSearch indices</h4>



| Description                                     	| Endpoint                               	| Request Body                                      	| Response Example                                                                	|
|-------------------------------------------------	|----------------------------------------	|---------------------------------------------------	|---------------------------------------------------------------------------------	|
| Check if a skill exists in ElasticSearch        	| /lookup_api/es_lookup/exact_attribute/ 	| ```{"attribute":"skill", "keyword": str}```       	| ``` {"list":[{"skill":"python","count":11413,"score":11.877036}]}```            	|
| Check if a job title exists in ElasticSearch    	| /lookup_api/es_lookup/exact_attribute/ 	| ```{"attribute":"jobTitle", "keyword": str}```    	| ``` {"list":[{"jobTitle":"java developer","count":20038,"score":11.877036}]}``` 	|
| Check if a company name exists in ElasticSearch 	| /lookup_api/es_lookup/exact_attribute/ 	| ```{"attribute":"companyName", "keyword": str}``` 	| ``` {"list":[{"companyName":"infosys","count":11413,"score":11.877036}]}```     	|


<hr style="border-top: 1px solid black;">

<a name = 'Recommendation-Engine-Endpoints-(Similar)'></a>
<h1 style="text-align:center">Recommendation Engine Endpoints (Similar)</h1>


* Similar - if “python” is the input; the response would be all similar words available in the database [“pyhton”,”cython” etc.]
   
   
| Description               	| Endpoint                                    	| Request Body                                      	| Response Example 	|
|---------------------------	|---------------------------------------------	|---------------------------------------------------	|------------------	|
| Get Similar skills        	| /ai_recommendation_engine/similar_attribute/ 	| ```{"attribute":"skill", "keyword": str}```       	|                  	|
| Get Similar job titles    	| /ai_recommendation_engine/similar_attribute/ 	| ```{"attribute":"jobTitle", "keyword": str}```    	|                  	|
| Get Similar company names 	| /ai_recommendation_engine/similar_attribute/ 	| ```{"attribute":"companyName", "keyword": str}``` 	|                  	|


<hr style="border-top: 1px solid black;">

<a name='Recommendation-Engine-Endpoints-(Related)'></a>
<h1 style="text-align:center">Recommendation Engine Endpoints (Related)</h1>
    
    
* Related: example, if “java developer” is the input, response would be: ["software developer","java programmer"]
 
| Description               	| Endpoint                                    	| Request Body                                        	| Response Example 	|
|---------------------------	|---------------------------------------------	|-----------------------------------------------------	|------------------	|
| Get related skills        	| /ai_recommendation_engine/related_attribute/ 	| ```{"attribute":"skill", "input_list": []}```       	|                  	|
| Get related job titles    	| /ai_recommendation_engine/related_attribute/ 	| ```{"attribute":"jobTitle", "input_list": []}```    	|                  	|
| Get related company names 	| /ai_recommendation_engine/related_attribute/ 	| ```{"attribute":"companyName", "input_list": []}``` 	|                  	|


<hr style="border-top: 1px solid black;">


---


<a name='Parsers'></a>
# Parsers

---

## Info

WiseStep's complete collection of parsers using the power of data science and machine learning. These endpoints exposes all of our algorithms.



---

| Description                   | Endpoint                     | Request Body                                                                                                  | Response Example                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
|-------------------------------|------------------------------|---------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| API to parse job descriptions | /wisestep-parsers/jd-parser/ | ```{"text":"software engineer on contract with 45k-60k per year at tech mahindra in austin texas with L1 visa."}``` | ```{'parsed_jd': {'clientName': ['tech mahindra'],   'compensations': [{'currencies': ['USD'],     'details': [{'type': 'NA', 'value': ' 60k'},      {'type': 'NA', 'value': ' 45k'}],     'ranges': {'max': 60000, 'min': 45000}}],   'hiringManager': [' '],   'jobDescription': ['Job Description: \n'],   'jobTitles': ['software engineer'],   'jobType': ['contract'],   'locations': [],   'skills': ['software engineer', 'texas', 'l1'],   'workAuthorization': ['h', 'h 4', 'l', 'l1']}}``` |


<a name='Errors'></a>

## Errors
---

| Status Code | Meaning                                    | Troubleshooting                |
|-------------|--------------------------------------------|--------------------------------|
| 200         | Successful request and response.           | OK                             |
| 400         | Malformed parameters or other bad request. | Check payload key-value format |
| 403         | Forbidden, Unauthorised                    | Check Authentication key-value |
| 404         | Not found                                  | Check endpoints, URL           |